fs = require "fs"
proc = require "child_process"
zlib = require "zlib"
archiver = require "archiver"
exec = (cmd) -> proc.execSync cmd, stdio: "inherit"
distDir = "./dist"

getExecutableName = ->
  fs
    .readdirSync(".")
    .find RegExp::exec.bind /^clickachu.*/

compileDist = ->
  exec "rm -r #{distDir}" if fs.existsSync(distDir)
  exec "mkdir #{distDir}"

  execName = getExecutableName() or throw Error("no executable name found")
  exec "cp #{execName} #{distDir}/#{execName}"

createArchive = ->
  platform = "#{process.platform}-#{process.arch}"
  zipDir distDir, "clickachu-#{platform}.zip"

zipDir = (dirname, outname) ->
  output = fs.createWriteStream outname
  archive = archiver "zip", zlib: {level: 9}
  archive.on "error", (err) -> throw err
  archive.pipe output
  archive.directory dirname, off, null

  do archive.finalize

cmds = [
  "git pull"
  "npm i"
  "go build"
  compileDist
  "coffee -o dist/chext chext"
  createArchive
  "go clean"
]

cmds.forEach (cmd) -> do (cmd.call and cmd or -> exec cmd)

