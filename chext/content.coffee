cssGenerator = new CssSelectorGenerator
cssGenerator.setOptions {
  selectors: ['tag', 'id', 'class', 'nthchild']
  class_blacklist: [
    /^ng-tns-c.*/i,
  ],
}
getSelector = (el) -> cssGenerator.getSelector el
getElementType = (el) -> el.constructor.name

socket = new WebSocket 'ws://localhost:6060/ws'
socket.json = (data) -> socket.send JSON.stringify data

socket.onopen = ->
  listenEvents = [
    'click'
    'keydown'
    'change'
  ]

  payloadResolvers = {
    keydown: (e) -> {
      code: e.code
      key: e.key
    }

    change: (e) -> {
      value: e.target.value
      type: e.target.type
    }
  }

  getPayloadFor = (eventName, event) -> payloadResolvers[eventName] event if payloadResolvers[eventName]

  eventHooks =
    click:
      isRejected: (e) -> e.detail is 0

    change:
      isRejected: (e) -> e.target.type in ['checkbox', 'radio']

  addEvent = (eventName, event) ->
    hooks = eventHooks[eventName]
    if hooks
      return if hooks.isRejected and hooks.isRejected event

    socket.json {
      eventName
      element: getSelector event.target
      payload: getPayloadFor eventName, event
      elementType: getElementType event.target
    }

  listenEvents.forEach (eventName) ->
    document.addEventListener eventName, ((e) -> addEvent eventName, e), true
