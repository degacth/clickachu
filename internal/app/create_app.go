package app

import (
	"clickachu/internal/optimize"
	"clickachu/internal/play"
	"clickachu/internal/record"
	"github.com/urfave/cli"
	"log"
)

const DefaultTimeout = 20

type flags_ struct {
	From    string
	Timeout int
}

var flags = flags_{}

var fromFlag = cli.StringFlag{
	Name:        "from",
	Usage:       "From to start record",
	Destination: &flags.From,
}

func getRecordName(c *cli.Context) string {
	recordName := c.Args().Get(0)
	if recordName == "" {
		log.Fatal("You should set name argument for record")
	}

	return recordName
}

func CreateApp() *cli.App {
	cliApp := cli.NewApp()
	cliApp.Name = "ClickChu"
	cliApp.Usage = "Stop them suffer"

	cliApp.Commands = []cli.Command{
		{
			Name:    "record",
			Aliases: []string{"r", "rec"},
			Usage:   "Record user actions",
			Action: func(c *cli.Context) {
				recordName := getRecordName(c)

				record.Record(&record.RunOptions{
					From: flags.From,
					Name: recordName,
				})
			},
			Flags: []cli.Flag{fromFlag},
		},

		{
			Name:    "play",
			Aliases: []string{"p", "pl"},
			Usage:   "Play record",
			Action: func(c *cli.Context) {
				recordName := getRecordName(c)
				play.Play(play.Options{
					Name:    recordName,
					Timeout: flags.Timeout,
				})
			},
			Flags: []cli.Flag{
				fromFlag,
				cli.IntFlag{
					Name:        "timeout, t",
					Usage:       "max time for wait element",
					Destination: &flags.Timeout,
					Value:       DefaultTimeout,
				},
			},
		},

		{
			Name:    "optimize",
			Aliases: []string{"opt", "o"},
			Usage:   "Optimize record",
			Action: func(c *cli.Context) {
				optimize.Optimize(&optimize.Options{
					Name: getRecordName(c),
				})
			},
		},
	}

	return cliApp
}
