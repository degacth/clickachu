package common

import (
	"context"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/runner"
	"time"
)

var FirstRunOpt = runner.Flag("no-first-run", "")
var IgnoreCertificateOpt = runner.Flag("ignore-certificate-errors", "")
var NewWindowOpt = runner.Flag("new-window", "")
var DisableInfobarsOpt = runner.Flag("disable-infobars", "")

func RecordExtensionOpt() runner.CommandLineOption {
	return runner.Flag("load-extension", Config.ExtensionDir)
}

func UserDataDirOpt() runner.CommandLineOption {
	return runner.Flag("user-data-dir", Config.UserDir+"/user-data")
}

func Browser(ctx context.Context, cmdOpts ...runner.CommandLineOption) (browser *chromedp.CDP) {
	browser, err := chromedp.New(ctx,
		chromedp.WithRunnerOptions(cmdOpts...),
	)
	Try(err).Panic()

	return
}

func WithBrowserCtx(cb func(br *chromedp.CDP, ctx context.Context), opts []runner.CommandLineOption) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	br := Browser(ctx, opts...)
	cb(br, ctx)
	_ = br.Shutdown(ctx)
	_ = br.Wait()
}

func BrowserTo(br *chromedp.CDP, url string, timeout int) {
	ctx, cnl := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	defer cnl()
	Try(
		br.Run(ctx, chromedp.Navigate(url))).
		Panic()
}
