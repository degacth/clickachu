package common

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
)

var execDir string
var userDir string
var Config configType
var env = os.Getenv("CLICKACHU_ENV")

func init() {
	initializer, ok := envInitializers[env]
	if ok {
		initializer()
	} else {
		envInitializers[""]()
	}

	Config = configType{
		ExtensionDir: execDir + "/chext",
		RecordsDir:   userDir + "/records",
		UserDir:      userDir,
	}

	createDirIsNotExists(Config.RecordsDir)
}

type configType struct {
	ExtensionDir string
	RecordsDir   string
	UserDir      string
}

func createDirIsNotExists(dirname string) {
	if _, err := os.Stat(dirname); os.IsNotExist(err) {
		Try(os.MkdirAll(dirname, 0755)).Panic()
	}
}

type envInitializer func()

var envInitializers = map[string]envInitializer{
	"dev": func() {
		if flag.Lookup("test.v") != nil {
			_ = os.Chdir(fmt.Sprintf("%s/src/clickachu", os.Getenv("GOPATH")))
		}

		workDir, _ := os.Getwd()
		execDir = workDir
		userDir = workDir
	},
	"": func() {
		exec, err := os.Executable()
		Try(err).Panic()
		execDir = filepath.Dir(exec)
		user_, err := user.Current()
		Try(err).Panic()
		userDir = user_.HomeDir + "/.clickachu"
	},
}

func isDev() bool {
	return env == "dev"
}
