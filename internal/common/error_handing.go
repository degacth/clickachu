package common

type ErrorResult struct {
	e error
}

func (e *ErrorResult) Panic() {
	if e.e != nil {
		L.Panic(e.e)
	}
}

func (e *ErrorResult) PanicWith(items ...interface{}) {
	if e.e != nil {
		L.Panic(items ...)
	}
}

func (e *ErrorResult) Warn() bool {
	if e.e == nil {
		return false
	}
	L.Println(e.e.Error())
	return true
}

func Try(err error) *ErrorResult {
	return &ErrorResult{err}
}
