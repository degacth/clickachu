package common

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTry(t *testing.T) {
	defer func() {
		e := recover()
		assert.Equal(t, e != nil, true)
	}()

	err := mayBeError()
	Try(err).Panic()
}

func mayBeError() error {
	return errors.New("oh! fuck")
}
