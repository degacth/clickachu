package common

func StringToKeyEvents(v, el string) Events {
	strLen := len(v)
	runes := []rune(v)
	events := make(Events, strLen, strLen+1)

	for i := 0; i < strLen; i++ {
		events[i] = &Event{
			Element:     el,
			EventName:   SupportedEvents.Key,
			ElementType: el,
		}

		events[i].SetPayload(NewKeyPayload("", string(runes[0])))
	}

	return events
}

func StringToChangeEvent(v, el string) *Event {
	e := &Event{
		Element:   el,
		EventName: SupportedEvents.Change,
	}

	e.SetPayload(NewChangePayload("", v))
	return e
}

func UserInputText(v, el string) Events {
	events := append(Events{}, StringToKeyEvents(v, el)...)
	return append(events, StringToChangeEvent(v, el))
}

func ClickEvents(selectors ...string) Events {
	itemLen := len(selectors)
	events := make(Events, itemLen, itemLen)

	for i, selector := range selectors {
		events[i] = &Event{
			Element:   selector,
			EventName: SupportedEvents.Click,
		}
	}

	return events
}
