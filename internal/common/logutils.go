package common

import (
	"log"
	"os"
)

var L *log.Logger

func init() {
	if isDev() {
		L = log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)
	} else {
		file, err := os.Create(Config.RecordsDir + "/project.log")
		if err != nil {
			log.Fatal(err)
		}

		L = log.New(file, "", log.LstdFlags|log.Lshortfile)
	}
}
