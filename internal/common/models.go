package common

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
)

type supportedEvents struct {
	Change string
	Key    string
	Click  string
}

var SupportedEvents = &supportedEvents{
	Click:  "click",
	Key:    "keydown",
	Change: "change",
}

type Event struct {
	sync.Mutex
	EventName     string                 `json:"eventName"`
	Element       string                 `json:"element"`
	ElementType   string                 `json:"elementType"`
	Payload       map[string]interface{} `json:"payload"`
	parsedPayload interface{}
}

func (e *Event) SetPayload(payload interface{}) *Event {
	e.parsedPayload = payload
	return e
}

func (e *Event) KeyPayload() *KeyPayload {
	e.Lock()
	defer e.Unlock()
	if e.parsedPayload == nil {
		p := e.Payload
		e.parsedPayload = &KeyPayload{
			Code: getMapValue(p, "code"),
			Key:  getMapValue(p, "key"),
		}
	}

	return e.parsedPayload.(*KeyPayload)
}

func (e *Event) ChangePayload() *ChangePayload {
	e.Lock()
	defer e.Unlock()

	if e.parsedPayload == nil {
		p := e.Payload
		e.parsedPayload = &ChangePayload{
			Type:  getMapValue(p, "type"),
			Value: getMapValue(p, "value"),
		}
	}

	return e.parsedPayload.(*ChangePayload)
}

type Events = []*Event

func EventFromBytes(data []byte) *Event {
	e := new(Event)
	Try(json.Unmarshal(data, e)).Panic()
	return e
}

type Vars = map[string]string

type Record struct {
	From   string `json:"from"`
	Events Events `json:"events"`
	Vars   Vars   `json:"vars"`
}

type KeyPayload struct {
	Code string `json:"code"`
	Key  string `json:"key"`
}

func NewKeyPayload(code string, key string) *KeyPayload {
	return &KeyPayload{code, key}
}

type ChangePayload struct {
	Type  string
	Value string
}

func NewChangePayload(type_, value string) *ChangePayload {
	return &ChangePayload{type_, value}
}

func getMapValue(m map[string]interface{}, key string) string {
	return fmt.Sprintf("%v", m[key])
}

func NameToVar(name string) string {
	return fmt.Sprintf("$$%s$$", name)
}

func GetValueFromVars(vars Vars, val string) string {
	varName := getVarName(val)
	if varName == val {
		return val
	}

	return vars[varName]
}

func getVarName(val string) string {
	return strings.Trim(val, "$$")
}
