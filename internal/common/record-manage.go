package common

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func GetRecordFromFile(name string) (record *Record, err error) {
	jsonFile, err := os.Open(fmt.Sprintf("%s/%s.json", Config.RecordsDir, name))
	if err != nil {
		return
	}
	defer jsonFile.Close()

	record = &Record{}
	recordData, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(recordData, record)
	return
}

func SquashEvents(events Events) Events {
	return squashKeysToChange(events)
}

func squashKeysToChange(events Events) (squashed Events) {
	groupKeysToChange := func(events Events) Events {
		head, tail := events[0], events[1:]
		commonEvents := Events{head}
		for _, event := range tail {
			if event.Element != head.Element {
				break
			}

			eventName := event.EventName

			if eventName == SupportedEvents.Key {
				commonEvents = append(commonEvents, event)
				continue
			}

			if eventName == SupportedEvents.Change {
				commonEvents = append(commonEvents, event)
			}

			break
		}

		return commonEvents
	}

	for i := 0; i < len(events); {
		event := events[i]
		if event.EventName == SupportedEvents.Key {
			grouped := groupKeysToChange(events[i:])
			groupedLen := len(grouped)
			lastGrouped := grouped[groupedLen-1]
			initGrouped := grouped[:groupedLen-1]
			squashed = append(squashed, lastGrouped)
			squashed = append(squashed, lastSpecialKyes(initGrouped)...)
			i += groupedLen
			continue
		}
		squashed = append(squashed, event)
		i++
	}

	return
}

func IsSpecialKey(key string) bool {
	if len(key) == 1 {
		return false
	}

	return true
}

func lastSpecialKyes(events Events) (specialEvents Events) {
	for i := len(events) - 1; i >= 0; i-- {
		event := events[i]
		if event.EventName != SupportedEvents.Key {
			continue
		}

		p := event.KeyPayload()
		if !IsSpecialKey(p.Key) {
			break
		}

		specialEvents = append(specialEvents, event)
	}
	return
}

func WriteRecord(r *Record, name string) error {
	buf := make([]byte, 0)
	out := bytes.NewBuffer(buf)
	enc := json.NewEncoder(out)
	enc.SetIndent("", "  ")
	enc.SetEscapeHTML(false)

	Try(enc.Encode(r)).Panic()

	return ioutil.WriteFile(
		fmt.Sprintf("%s/%s.json", Config.RecordsDir, name),
		out.Bytes(), 0644)
}
