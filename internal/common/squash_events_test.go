package common

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSquashEvents(t *testing.T) {
	value := "hello"
	element := "a"
	changeEvent := StringToChangeEvent(value, element)
	events := append(StringToKeyEvents(value, element), changeEvent)
	events = SquashEvents(events)

	assert.Equal(t, 1, len(events))
	assert.Equal(t, changeEvent, events[0])
}
