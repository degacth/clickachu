package common

import (
	"context"
	"fmt"
	"github.com/phayes/freeport"
	"html/template"
	"net/http"
)

const templatesDir = "./internal/common/test-templates"

func WithTemplateServerCtx(cb func(addr string)) {
	port, _ := freeport.GetFreePort()
	server := &http.Server{
		Addr: fmt.Sprintf("0.0.0.0:%d", port),
	}

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		templateName := request.URL.Path[1:]

		tpl, err := template.ParseFiles(
			templatesDir+"/base-layout.html",
			templatesDir+fmt.Sprintf("/%s.html", templateName))

		if err != nil {
			http.NotFound(writer, request)
			return
		}

		Try(
			tpl.ExecuteTemplate(writer, "content", nil)).
			Panic()
	})

	go func() {
		if err := server.ListenAndServe(); http.ErrServerClosed != err {
			Try(err).Panic()
		}
	}()

	cb(fmt.Sprintf("http://%s", server.Addr))

	Try(
		server.Shutdown(context.Background())).
		Panic()
}
