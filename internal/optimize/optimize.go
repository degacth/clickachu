package optimize

import (
	"clickachu/internal/common"
	"github.com/Pallinder/go-randomdata"
	"strings"
)

type Options struct {
	Name string
}

func Optimize(opt *Options) {
	record, err := common.GetRecordFromFile(opt.Name)
	common.Try(err).Panic()
	common.Try(
		common.WriteRecord(
			optimizeRecord(record), opt.Name)).
		Panic()
}

func optimizeRecord(record *common.Record) *common.Record {
	record.Events = common.SquashEvents(record.Events)
	updateEventsWithVars(record)
	return record
}

func updateEventsWithVars(record *common.Record) {
	vars := record.Vars
	if vars == nil {
		vars = common.Vars{}
	}

	uniqSelectors := map[string]string{}
	var getUniqName func() string

	getUniqName = func() string {
		name := getVarName()
		if _, ok := vars[name]; ok {
			return getUniqName()
		}
		return name
	}

	for _, event := range record.Events {
		if _, ok := uniqSelectors[event.Element]; !ok {
			name := getUniqName()
			vars[name] = event.Element
			uniqSelectors[event.Element] = name
		}

		event.Element = common.NameToVar(uniqSelectors[event.Element])
	}

	record.Vars = vars
}

func getVarName() string {
	return strings.Replace(randomdata.FullName(randomdata.RandomGender), " ", "", -1)
}
