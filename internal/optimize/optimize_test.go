package optimize

import (
	"clickachu/internal/common"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOptimize(t *testing.T) {
	userInput := "test"
	userInputEl := "div input"

	var events common.Events
	events = append(events, common.StringToKeyEvents(userInput, userInputEl)...)
	events = append(events, common.StringToChangeEvent(userInput, userInputEl))
	events = append(events, common.ClickEvents("body", "body")...)

	record := &common.Record{
		Events: events,
	}

	optimizeRecord(record)
	assert.Equal(t, 3, len(record.Events))
	assert.Equal(t, 2, len(record.Vars))

	var varName string
	for varName = range record.Vars {
		break
	}

	assert.NotContains(t, varName, " ")

	assert.Equal(t, record.Events[0].Element, common.NameToVar(varName))
}
