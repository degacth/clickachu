package play

import (
	"clickachu/internal/common"
	"context"
	"errors"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/kb"
	"github.com/chromedp/chromedp/runner"
	"time"
)

type Options struct {
	Name    string
	Timeout int
}

func Play(opts Options) {
	record, err := common.GetRecordFromFile(opts.Name)
	common.Try(err).Panic()

	ctx, cnl := context.WithCancel(context.Background())
	defer cnl()

	br := common.Browser(ctx, []runner.CommandLineOption{
		common.NewWindowOpt,
		common.DisableInfobarsOpt,
		common.IgnoreCertificateOpt,
		common.UserDataDirOpt(),
	}...)

	common.Try(br.Run(ctx,
		chromedp.Tasks{
			chromedp.Navigate(record.From),
		})).Panic()

	runEvents(br, record.Vars, record.Events, time.Duration(opts.Timeout)*time.Second)

	_ = br.Wait()
}

func runEvents(br *chromedp.CDP, vars common.Vars, events common.Events, timeout time.Duration) {
	// long timeout chromedp/handler.go WaitNode method :477 timeout := time.After(10 * time.Second)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	events = common.SquashEvents(events)
	for _, event := range events {
		var task chromedp.Action
		timeoutCtx, cnl := context.WithTimeout(ctx, timeout)

		selector := common.GetValueFromVars(vars, event.Element)

	eventSwitch:
		switch event.EventName {
		case common.SupportedEvents.Click:
			task = chromedp.Click(selector, chromedp.ByQuery)

		case common.SupportedEvents.Key:
			payload := event.KeyPayload()
			if common.IsSpecialKey(payload.Key) {
				switch payload.Key {
				case "Enter":
					task = chromedp.SendKeys(selector, kb.Enter, chromedp.ByQuery)
					break eventSwitch
				}
			}
			continue
		case common.SupportedEvents.Change:
			payload := event.ChangePayload()
			task = chromedp.SendKeys(selector, payload.Value, chromedp.ByQuery)
		default:
			panic(errors.New("Unsupported event " + event.EventName))
		}

		common.Try(
			br.Run(timeoutCtx, chromedp.Tasks{
				task,
			})).
			PanicWith("no item with ", event.EventName, " : ", selector)

		cnl()
	}
}
