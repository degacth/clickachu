package play

import (
	"clickachu/internal/common"
	"context"
	"fmt"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/runner"
	"testing"
	"time"
)

func TestPlay(t *testing.T) {
	common.WithTemplateServerCtx(func(addr string) {
		startUrl := fmt.Sprintf("%s/%s", addr, "events-recorder")
		common.WithBrowserCtx(func(br *chromedp.CDP, ctx context.Context) {

			common.BrowserTo(br, startUrl, 2)
			events := append(common.Events{},
				append(common.ClickEvents(".link", ".no-meter-click"),
					common.UserInputText("test text", "#test-input-text")..., )...,
			)
			runEvents(br, common.Vars{}, events, time.Second*3)

		}, []runner.CommandLineOption{
			common.FirstRunOpt,
			common.NewWindowOpt,
			common.DisableInfobarsOpt,
		})
	})
}
