package play

import "clickachu/internal/common"

var stubs = map[string]struct {
	templateName string
	events       []common.Event
}{
	"simple-test": {
		templateName: "events-recorder",
		events: []common.Event{
			{
				EventName: "click",
				Element:   ".link",
			},
		},
	},
}
