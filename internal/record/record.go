package record

import (
	"clickachu/internal/common"
	"context"
	"github.com/chromedp/chromedp"
)

type RunOptions struct {
	From string
	Name string
}

func Record(opts *RunOptions) {
	recordDone := make(chan interface{})
	eventsStream := record(recordDone)
	var events common.Events

	go func() {
		for event := range eventsStream {
			events = append(events, event)
		}
	}()

	ctx, cnl := context.WithCancel(context.Background())
	defer cnl()

	br := common.Browser(ctx,
		common.FirstRunOpt,
		common.RecordExtensionOpt(),
		common.IgnoreCertificateOpt,
		common.DisableInfobarsOpt,
		common.NewWindowOpt,
	)

	err := br.Run(ctx, chromedp.Navigate(opts.From))
	common.Try(err).Panic()

	common.Try(
		br.Wait()).Panic()

	close(recordDone)

	common.Try(
		common.WriteRecord(&common.Record{
			From:   opts.From,
			Events: events,
		}, opts.Name),
	).Panic()
}

func record(done <-chan interface{}) <-chan *common.Event {
	eventsStream := make(chan *common.Event)

	rawEvents := make(chan []byte)
	go serve(rawEvents, done)
	go func() {
	eventsLoop:
		for {
			select {
			case eventsStream <- common.EventFromBytes(<-rawEvents):
			case <-done:
				break eventsLoop
			}
		}

		close(rawEvents)
	}()

	return eventsStream
}
