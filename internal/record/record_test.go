package record

import (
	"clickachu/internal/common"
	"context"
	"fmt"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/runner"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRecord(t *testing.T) {
	var events []common.Event

	common.WithTemplateServerCtx(func(addr string) {
		println(common.Config.ExtensionDir)
		common.WithBrowserCtx(func(br *chromedp.CDP, ctx context.Context) {

			done := make(chan interface{})
			eventsStream := record(done)
			go func() {
				for event := range eventsStream {
					events = append(events, event)
				}
			}()

			startUrl := fmt.Sprintf("%s/%s", addr, "events-recorder")
			common.BrowserTo(br, startUrl, 2)
			err := br.Run(ctx, chromedp.Tasks{
				chromedp.Click(".link"),
				chromedp.Click(".label"),
				chromedp.SendKeys(".label input", "foo"),
				chromedp.Click(".no-meter-click"),
				chromedp.Click("[for=test-checkbox]"),
			})
			common.Try(err).Panic()

			close(done)

		}, []runner.CommandLineOption{
			common.RecordExtensionOpt(),
			common.FirstRunOpt,
		})

		assert.Equal(t, 8, len(events))

		keydownEvent := events[4]
		assert.Equal(t, keydownEvent.EventName, "keydown")

		changeEvent := events[5]
		assert.Equal(t, changeEvent.EventName, "change")

		clickEvent := events[6]
		assert.Equal(t, clickEvent.EventName, "click")
	})
}
