package record

import (
	"clickachu/internal/common"
	"github.com/gorilla/websocket"
	"net/http"
)

func serve(events chan<- []byte, done <-chan interface{}) {
	var upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var server = http.Server{
		Addr: ":6060",
	}

	http.HandleFunc("/ws", func(writer http.ResponseWriter, request *http.Request) {
		conn, err := upgrader.Upgrade(writer, request, nil)
		common.Try(err).Panic()

		defer func() {
			_ = common.Try(
				conn.Close()).Warn()
		}()

		for {
			mt, message, err := conn.ReadMessage()

			if websocket.IsCloseError(err, 1001) {
				break
			}

			if common.Try(err).Warn() {
				break
			}

			if mt != websocket.TextMessage {
				common.L.Println("socket message type err", mt)
				break
			}

			events <- message
		}
	})

	go func() {
		<-done
		common.Try(
			server.Close()).Warn()
	}()

	err := server.ListenAndServe()
	if err == nil || err == http.ErrServerClosed {
		return
	}
	common.Try(err).Panic()
}
