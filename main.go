package main

import (
	"clickachu/internal/app"
	"os"
)

func main() {
	_ = app.CreateApp().
		Run(os.Args)
}
